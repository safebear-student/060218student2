package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 06/02/2018.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin(){
        //Step 01 confirm we're on welcome page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 02 click the Login link and the Login Page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        //step 03 test user login
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }
}
