package com.safebear.app;

import com.safebear.app.pages.FramesPage;
import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

/**
 * Created by CCA_Student on 06/02/2018.
 */
public class BaseTest {

    WebDriver driver;
    WelcomePage welcomePage;
    LoginPage loginPage;
    FramesPage framesPage;
    UserPage userPage;

    @Before
    public void setup() {
        //driver = new ChromeDriver();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        this.driver = new ChromeDriver(chromeOptions);
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        framesPage = new FramesPage(driver);
        userPage = new UserPage(driver);
        driver.get("http://automate.safebear.co.uk/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }


    @After
    public void tearDown(){
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.close();

    }
}
